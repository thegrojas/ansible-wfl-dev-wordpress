<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-VERSION-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-TODO-blue.svg)](/LICENSE)

</div>

---

# Ansible Workflow - Development - Wordpress

This role does the following:

- Installs composer via `geerlingguy.composer` Ansible Role.
- Installs Local (By Flywheel).
- Installs FileZilla.

## 🦺 Requirements

*None*

## 🗃️ Role Variables

*TODO*

## 📦 Dependencies

- `geerlingguy.composer` Role.

## 🤹 Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: workstations
      roles:
         - ansible-wlf-dev-wordpress
```

## ⚖️ License

*TODO*

## ✍️ Author Information

*TODO*
